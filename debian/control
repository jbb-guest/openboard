Source: openboard
Section: x11
Priority: optional
Maintainer: Debian Edu Packaging Team <debian-edu-pkg-team@lists.alioth.debian.org>
Uploaders:
 Mike Gabriel <sunweaver@debian.org>,
 Benjamin Schlüter <bschlueter@posteo.de>,
Build-Depends: cdbs,
               debhelper (>= 10),
               dpkg-dev (>= 1.16.1.1),
               libasound2-dev,
               libass-dev,
               libavformat-dev,
               libavcodec-dev,
               libbz2-dev,
               libfreetype6-dev,
               liblzma-dev,
               libmp3lame-dev,
               libopus-dev,
               libogg-dev,
               libpoppler-private-dev,
               libpoppler-dev,
               libquazip5-dev,
               libquazip-headers,
               libqt5svg5-dev,
               libqt5webkit5-dev,
               libqt5xmlpatterns5-dev,
               libsdl-dev,
               libssl-dev,
               libswscale-dev,
               libtheora-dev,
               libva-dev,
               libvorbis-dev,
               libvpx-dev,
               libx264-dev,
               libxcb-shm0-dev,
               lsb-release,
               qt5-qmake,
               qtbase5-dev,
               qtmultimedia5-dev,
               qtscript5-dev,
               qttools5-dev-tools,
               qttools5-private-dev,
               zlib1g-dev,
Standards-Version: 4.2.1
Vcs-Git: https://salsa.debian.org/debian-edu-pkg-team/openboard.git
Vcs-Browser: https://salsa.debian.org/debian-edu-pkg-team/openboard
Homepage: https://github.com/OpenBoard-org/OpenBoard

Package: openboard
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         openboard-common (>= ${source:Version}),
         fonts-ecolier-court,
         fonts-ecolier-lignes-court,
         fonts-sil-andika,
Description: Interactive White Board Application
 OpenBoard is an open source cross-platform interactive white board
 application designed primarily for use in schools.
 .
 It was originally forked from Open-Sankoré, which was itself based on
 Uniboard.

Package: openboard-common
Architecture: all
Depends: ${misc:Depends},
 libjs-jquery,
 libjs-jquery-colorpicker,
 libjs-jquery-easing,
 libjs-jquery-i18n-properties,
 libjs-jquery-ui,
 libjs-modernizr,
 libjs-mustache,
Description: Interactive White Board Application (common files)
 OpenBoard is an open source cross-platform interactive white board
 application designed primarily for use in schools.
 .
 It was originally forked from Open-Sankoré, which was itself based on
 Uniboard.
 .
 This package contains all architecture independent files.
