--- /dev/null
+++ b/thirdparty/qtsingleapplication/qtlocalpeer.cpp
@@ -0,0 +1,179 @@
+/****************************************************************************
+**
+** Copyright (C) 2015 The Qt Company Ltd.
+** Contact: http://www.qt.io/licensing
+**
+** This file is part of Qt Creator.
+**
+** Commercial License Usage
+** Licensees holding valid commercial Qt licenses may use this file in
+** accordance with the commercial license agreement provided with the
+** Software or, alternatively, in accordance with the terms contained in
+** a written agreement between you and The Qt Company.  For licensing terms and
+** conditions see http://www.qt.io/terms-conditions.  For further information
+** use the contact form at http://www.qt.io/contact-us.
+**
+** GNU Lesser General Public License Usage
+** Alternatively, this file may be used under the terms of the GNU Lesser
+** General Public License version 2.1 or version 3 as published by the Free
+** Software Foundation and appearing in the file LICENSE.LGPLv21 and
+** LICENSE.LGPLv3 included in the packaging of this file.  Please review the
+** following information to ensure the GNU Lesser General Public License
+** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
+** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
+**
+** In addition, as a special exception, The Qt Company gives you certain additional
+** rights.  These rights are described in The Qt Company LGPL Exception
+** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
+**
+****************************************************************************/
+
+#include "qtlocalpeer.h"
+
+#include <QCoreApplication>
+#include <QDataStream>
+#include <QTime>
+
+#if defined(Q_OS_WIN)
+#include <QLibrary>
+#include <qt_windows.h>
+typedef BOOL(WINAPI*PProcessIdToSessionId)(DWORD,DWORD*);
+static PProcessIdToSessionId pProcessIdToSessionId = 0;
+#endif
+
+#if defined(Q_OS_UNIX)
+#include <time.h>
+#include <unistd.h>
+#endif
+
+static const char ack[] = "ack";
+
+QString QtLocalPeer::appSessionId(const QString &appId)
+{
+    QByteArray idc = appId.toUtf8();
+    quint16 idNum = qChecksum(idc.constData(), idc.size());
+    //### could do: two 16bit checksums over separate halves of id, for a 32bit result - improved uniqeness probability. Every-other-char split would be best.
+
+    QString res = QLatin1String("qtsingleapplication-")
+                 + QString::number(idNum, 16);
+#if defined(Q_OS_WIN)
+    if (!pProcessIdToSessionId) {
+        QLibrary lib(QLatin1String("kernel32"));
+        pProcessIdToSessionId = (PProcessIdToSessionId)lib.resolve("ProcessIdToSessionId");
+    }
+    if (pProcessIdToSessionId) {
+        DWORD sessionId = 0;
+        pProcessIdToSessionId(GetCurrentProcessId(), &sessionId);
+        res += QLatin1Char('-') + QString::number(sessionId, 16);
+    }
+#else
+    res += QLatin1Char('-') + QString::number(::getuid(), 16);
+#endif
+    return res;
+}
+
+QtLocalPeer::QtLocalPeer(QObject *parent, const QString &appId)
+    : QObject(parent), id(appId)
+{
+    if (id.isEmpty())
+        id = QCoreApplication::applicationFilePath();  //### On win, check if this returns .../argv[0] without casefolding; .\MYAPP == .\myapp on Win
+
+    socketName = appSessionId(id);
+    server = new QLocalServer(this);
+    QString lockName = QDir(QDir::tempPath()).absolutePath()
+                       + QLatin1Char('/') + socketName
+                       + QLatin1String("-lockfile");
+    lockFile.setFileName(lockName);
+    lockFile.open(QIODevice::ReadWrite);
+}
+
+bool QtLocalPeer::isClient()
+{
+    if (lockFile.isLocked())
+        return false;
+
+    if (!lockFile.lock(QtLockedFile::WriteLock, false))
+        return true;
+
+    if (!QLocalServer::removeServer(socketName))
+        qWarning("QtSingleCoreApplication: could not cleanup socket");
+    bool res = server->listen(socketName);
+    if (!res)
+        qWarning("QtSingleCoreApplication: listen on local socket failed, %s", qPrintable(server->errorString()));
+    QObject::connect(server, SIGNAL(newConnection()), SLOT(receiveConnection()));
+    return false;
+}
+
+bool QtLocalPeer::sendMessage(const QString &message, int timeout, bool block)
+{
+    if (!isClient())
+        return false;
+
+    QLocalSocket socket;
+    bool connOk = false;
+    for (int i = 0; i < 2; i++) {
+        // Try twice, in case the other instance is just starting up
+        socket.connectToServer(socketName);
+        connOk = socket.waitForConnected(timeout/2);
+        if (connOk || i)
+            break;
+        int ms = 250;
+#if defined(Q_OS_WIN)
+        Sleep(DWORD(ms));
+#else
+        struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
+        nanosleep(&ts, NULL);
+#endif
+    }
+    if (!connOk)
+        return false;
+
+    QByteArray uMsg(message.toUtf8());
+    QDataStream ds(&socket);
+    ds.writeBytes(uMsg.constData(), uMsg.size());
+    bool res = socket.waitForBytesWritten(timeout);
+    res &= socket.waitForReadyRead(timeout); // wait for ack
+    res &= (socket.read(qstrlen(ack)) == ack);
+    if (block) // block until peer disconnects
+        socket.waitForDisconnected(-1);
+    return res;
+}
+
+void QtLocalPeer::receiveConnection()
+{
+    QLocalSocket* socket = server->nextPendingConnection();
+    if (!socket)
+        return;
+
+    // Why doesn't Qt have a blocking stream that takes care of this shait???
+    while (socket->bytesAvailable() < static_cast<int>(sizeof(quint32))) {
+        if (!socket->isValid()) // stale request
+            return;
+        socket->waitForReadyRead(1000);
+    }
+    QDataStream ds(socket);
+    QByteArray uMsg;
+    quint32 remaining;
+    ds >> remaining;
+    uMsg.resize(remaining);
+    int got = 0;
+    char* uMsgBuf = uMsg.data();
+    //qDebug() << "RCV: remaining" << remaining;
+    do {
+        got = ds.readRawData(uMsgBuf, remaining);
+        remaining -= got;
+        uMsgBuf += got;
+        //qDebug() << "RCV: got" << got << "remaining" << remaining;
+    } while (remaining && got >= 0 && socket->waitForReadyRead(2000));
+    //### error check: got<0
+    if (got < 0) {
+        qWarning() << "QtLocalPeer: Message reception failed" << socket->errorString();
+        delete socket;
+        return;
+    }
+    // ### async this
+    QString message = QString::fromUtf8(uMsg.constData(), uMsg.size());
+    socket->write(ack, qstrlen(ack));
+    socket->waitForBytesWritten(1000);
+    emit messageReceived(message, socket); // ##(might take a long time to return)
+}
--- /dev/null
+++ b/thirdparty/qtsingleapplication/qtlocalpeer.h
@@ -0,0 +1,62 @@
+/****************************************************************************
+**
+** Copyright (C) 2015 The Qt Company Ltd.
+** Contact: http://www.qt.io/licensing
+**
+** This file is part of Qt Creator.
+**
+** Commercial License Usage
+** Licensees holding valid commercial Qt licenses may use this file in
+** accordance with the commercial license agreement provided with the
+** Software or, alternatively, in accordance with the terms contained in
+** a written agreement between you and The Qt Company.  For licensing terms and
+** conditions see http://www.qt.io/terms-conditions.  For further information
+** use the contact form at http://www.qt.io/contact-us.
+**
+** GNU Lesser General Public License Usage
+** Alternatively, this file may be used under the terms of the GNU Lesser
+** General Public License version 2.1 or version 3 as published by the Free
+** Software Foundation and appearing in the file LICENSE.LGPLv21 and
+** LICENSE.LGPLv3 included in the packaging of this file.  Please review the
+** following information to ensure the GNU Lesser General Public License
+** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
+** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
+**
+** In addition, as a special exception, The Qt Company gives you certain additional
+** rights.  These rights are described in The Qt Company LGPL Exception
+** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
+**
+****************************************************************************/
+
+#include <qtlockedfile.h>
+
+#include <QLocalServer>
+#include <QLocalSocket>
+#include <QDir>
+
+
+class QtLocalPeer : public QObject
+{
+    Q_OBJECT
+
+public:
+    explicit QtLocalPeer(QObject *parent = 0, const QString &appId = QString());
+    bool isClient();
+    bool sendMessage(const QString &message, int timeout, bool block);
+    QString applicationId() const
+        { return id; }
+    static QString appSessionId(const QString &appId);
+
+Q_SIGNALS:
+    void messageReceived(const QString &message, QObject *socket);
+
+protected Q_SLOTS:
+    void receiveConnection();
+
+protected:
+    QString id;
+    QString socketName;
+    QLocalServer* server;
+    QtLockedFile lockFile;
+};
+
--- /dev/null
+++ b/thirdparty/qtsingleapplication/qtsingleapplication.cpp
@@ -0,0 +1,191 @@
+/****************************************************************************
+**
+** Copyright (C) 2015 The Qt Company Ltd.
+** Contact: http://www.qt.io/licensing
+**
+** This file is part of Qt Creator.
+**
+** Commercial License Usage
+** Licensees holding valid commercial Qt licenses may use this file in
+** accordance with the commercial license agreement provided with the
+** Software or, alternatively, in accordance with the terms contained in
+** a written agreement between you and The Qt Company.  For licensing terms and
+** conditions see http://www.qt.io/terms-conditions.  For further information
+** use the contact form at http://www.qt.io/contact-us.
+**
+** GNU Lesser General Public License Usage
+** Alternatively, this file may be used under the terms of the GNU Lesser
+** General Public License version 2.1 or version 3 as published by the Free
+** Software Foundation and appearing in the file LICENSE.LGPLv21 and
+** LICENSE.LGPLv3 included in the packaging of this file.  Please review the
+** following information to ensure the GNU Lesser General Public License
+** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
+** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
+**
+** In addition, as a special exception, The Qt Company gives you certain additional
+** rights.  These rights are described in The Qt Company LGPL Exception
+** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
+**
+****************************************************************************/
+
+#include "qtsingleapplication.h"
+#include "qtlocalpeer.h"
+
+#include <qtlockedfile.h>
+
+#include <QDir>
+#include <QFileOpenEvent>
+#include <QSharedMemory>
+#include <QWidget>
+
+
+static const int instancesSize = 1024;
+
+static QString instancesLockFilename(const QString &appSessionId)
+{
+    const QChar slash(QLatin1Char('/'));
+    QString res = QDir::tempPath();
+    if (!res.endsWith(slash))
+        res += slash;
+    return res + appSessionId + QLatin1String("-instances");
+}
+
+QtSingleApplication::QtSingleApplication(const QString &appId, int &argc, char **argv)
+    : QApplication(argc, argv),
+      firstPeer(-1),
+      pidPeer(0)
+{
+    this->appId = appId;
+
+    const QString appSessionId = QtLocalPeer::appSessionId(appId);
+
+    // This shared memory holds a zero-terminated array of active (or crashed) instances
+    instances = new QSharedMemory(appSessionId, this);
+    actWin = 0;
+    block = false;
+
+    // First instance creates the shared memory, later instances attach to it
+    const bool created = instances->create(instancesSize);
+    if (!created) {
+        if (!instances->attach()) {
+            qWarning() << "Failed to initialize instances shared memory: "
+                       << instances->errorString();
+            delete instances;
+            instances = 0;
+            return;
+        }
+    }
+
+    // QtLockedFile is used to workaround QTBUG-10364
+    QtLockedFile lockfile(instancesLockFilename(appSessionId));
+
+    lockfile.open(QtLockedFile::ReadWrite);
+    lockfile.lock(QtLockedFile::WriteLock);
+    qint64 *pids = static_cast<qint64 *>(instances->data());
+    if (!created) {
+        // Find the first instance that it still running
+        // The whole list needs to be iterated in order to append to it
+        for (; *pids; ++pids) {
+            if (firstPeer == -1 && isRunning(*pids))
+                firstPeer = *pids;
+        }
+    }
+    // Add current pid to list and terminate it
+    *pids++ = QCoreApplication::applicationPid();
+    *pids = 0;
+    pidPeer = new QtLocalPeer(this, appId + QLatin1Char('-') +
+                              QString::number(QCoreApplication::applicationPid()));
+    connect(pidPeer, SIGNAL(messageReceived(QString,QObject*)), SIGNAL(messageReceived(QString,QObject*)));
+    pidPeer->isClient();
+    lockfile.unlock();
+}
+
+QtSingleApplication::~QtSingleApplication()
+{
+    if (!instances)
+        return;
+    const qint64 appPid = QCoreApplication::applicationPid();
+    QtLockedFile lockfile(instancesLockFilename(QtLocalPeer::appSessionId(appId)));
+    lockfile.open(QtLockedFile::ReadWrite);
+    lockfile.lock(QtLockedFile::WriteLock);
+    // Rewrite array, removing current pid and previously crashed ones
+    qint64 *pids = static_cast<qint64 *>(instances->data());
+    qint64 *newpids = pids;
+    for (; *pids; ++pids) {
+        if (*pids != appPid && isRunning(*pids))
+            *newpids++ = *pids;
+    }
+    *newpids = 0;
+    lockfile.unlock();
+}
+
+bool QtSingleApplication::event(QEvent *event)
+{
+    if (event->type() == QEvent::FileOpen) {
+        QFileOpenEvent *foe = static_cast<QFileOpenEvent*>(event);
+        emit fileOpenRequest(foe->file());
+        return true;
+    }
+    return QApplication::event(event);
+}
+
+bool QtSingleApplication::isRunning(qint64 pid)
+{
+    if (pid == -1) {
+        pid = firstPeer;
+        if (pid == -1)
+            return false;
+    }
+
+    QtLocalPeer peer(this, appId + QLatin1Char('-') + QString::number(pid, 10));
+    return peer.isClient();
+}
+
+bool QtSingleApplication::sendMessage(const QString &message, int timeout, qint64 pid)
+{
+    if (pid == -1) {
+        pid = firstPeer;
+        if (pid == -1)
+            return false;
+    }
+
+    QtLocalPeer peer(this, appId + QLatin1Char('-') + QString::number(pid, 10));
+    return peer.sendMessage(message, timeout, block);
+}
+
+QString QtSingleApplication::applicationId() const
+{
+    return appId;
+}
+
+void QtSingleApplication::setBlock(bool value)
+{
+    block = value;
+}
+
+void QtSingleApplication::setActivationWindow(QWidget *aw, bool activateOnMessage)
+{
+    actWin = aw;
+    if (!pidPeer)
+        return;
+    if (activateOnMessage)
+        connect(pidPeer, SIGNAL(messageReceived(QString,QObject*)), this, SLOT(activateWindow()));
+    else
+        disconnect(pidPeer, SIGNAL(messageReceived(QString,QObject*)), this, SLOT(activateWindow()));
+}
+
+
+QWidget* QtSingleApplication::activationWindow() const
+{
+    return actWin;
+}
+
+
+void QtSingleApplication::activateWindow()
+{
+    if (actWin) {
+        actWin->setWindowState(actWin->windowState() & ~Qt::WindowMinimized);
+        actWin->raise();
+        actWin->activateWindow();
+    }
+}
--- /dev/null
+++ b/thirdparty/qtsingleapplication/qtsingleapplication.h
@@ -0,0 +1,73 @@
+/****************************************************************************
+**
+** Copyright (C) 2015 The Qt Company Ltd.
+** Contact: http://www.qt.io/licensing
+**
+** This file is part of Qt Creator.
+**
+** Commercial License Usage
+** Licensees holding valid commercial Qt licenses may use this file in
+** accordance with the commercial license agreement provided with the
+** Software or, alternatively, in accordance with the terms contained in
+** a written agreement between you and The Qt Company.  For licensing terms and
+** conditions see http://www.qt.io/terms-conditions.  For further information
+** use the contact form at http://www.qt.io/contact-us.
+**
+** GNU Lesser General Public License Usage
+** Alternatively, this file may be used under the terms of the GNU Lesser
+** General Public License version 2.1 or version 3 as published by the Free
+** Software Foundation and appearing in the file LICENSE.LGPLv21 and
+** LICENSE.LGPLv3 included in the packaging of this file.  Please review the
+** following information to ensure the GNU Lesser General Public License
+** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
+** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
+**
+** In addition, as a special exception, The Qt Company gives you certain additional
+** rights.  These rights are described in The Qt Company LGPL Exception
+** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
+**
+****************************************************************************/
+
+#include <QApplication>
+
+QT_FORWARD_DECLARE_CLASS(QSharedMemory)
+
+
+class QtLocalPeer;
+
+class QtSingleApplication : public QApplication
+{
+    Q_OBJECT
+
+public:
+    QtSingleApplication(const QString &id, int &argc, char **argv);
+    ~QtSingleApplication();
+
+    bool isRunning(qint64 pid = -1);
+
+    void setActivationWindow(QWidget* aw, bool activateOnMessage = true);
+    QWidget* activationWindow() const;
+    bool event(QEvent *event);
+
+    QString applicationId() const;
+    void setBlock(bool value);
+
+public Q_SLOTS:
+    bool sendMessage(const QString &message, int timeout = 5000, qint64 pid = -1);
+    void activateWindow();
+
+Q_SIGNALS:
+    void messageReceived(const QString &message, QObject *socket);
+    void fileOpenRequest(const QString &file);
+
+private:
+    QString instancesFileName(const QString &appId);
+
+    qint64 firstPeer;
+    QSharedMemory *instances;
+    QtLocalPeer *pidPeer;
+    QWidget *actWin;
+    QString appId;
+    bool block;
+};
+
--- /dev/null
+++ b/thirdparty/qtsingleapplication/qtsingleapplication.pri
@@ -0,0 +1,15 @@
+INCLUDEPATH += $$PWD
+DEPENDPATH += $$PWD
+QT *= network
+
+qtsingleapplication-uselib:!qtsingleapplication-buildlib {
+    LIBS += -L$$QTSINGLEAPPLICATION_LIBDIR -l$$QTSINGLEAPPLICATION_LIBNAME
+} else {
+    SOURCES += $$PWD/qtsingleapplication.cpp $$PWD/qtlocalpeer.cpp
+    HEADERS += $$PWD/qtsingleapplication.h $$PWD/qtlocalpeer.h
+}
+
+win32 {
+    contains(TEMPLATE, lib):contains(CONFIG, shared):DEFINES += QT_QTSINGLEAPPLICATION_EXPORT
+    else:qtsingleapplication-uselib:DEFINES += QT_QTSINGLEAPPLICATION_IMPORT
+}
--- a/OpenBoard.pro
+++ b/OpenBoard.pro
@@ -71,6 +71,10 @@ include(src/podcast/podcast.pri)
 include(src/tools/tools.pri)
 include(src/desktop/desktop.pri)
 include(src/web/web.pri)
+INCLUDEPATH += thirdparty/qtlockedfile
+include(thirdparty/qtlockedfile/qtlockedfile.pri)
+INCLUDEPATH += thirdparty/qtsingleapplication
+include(thirdparty/qtsingleapplication/qtsingleapplication.pri)
 
 DEPENDPATH += src/pdf-merger
 INCLUDEPATH += src/pdf-merger
--- /dev/null
+++ b/thirdparty/qtlockedfile/qtlockedfile.cpp
@@ -0,0 +1,155 @@
+/****************************************************************************
+**
+** Copyright (C) 2015 The Qt Company Ltd.
+** Contact: http://www.qt.io/licensing
+**
+** This file is part of Qt Creator.
+**
+** Commercial License Usage
+** Licensees holding valid commercial Qt licenses may use this file in
+** accordance with the commercial license agreement provided with the
+** Software or, alternatively, in accordance with the terms contained in
+** a written agreement between you and The Qt Company.  For licensing terms and
+** conditions see http://www.qt.io/terms-conditions.  For further information
+** use the contact form at http://www.qt.io/contact-us.
+**
+** GNU Lesser General Public License Usage
+** Alternatively, this file may be used under the terms of the GNU Lesser
+** General Public License version 2.1 or version 3 as published by the Free
+** Software Foundation and appearing in the file LICENSE.LGPLv21 and
+** LICENSE.LGPLv3 included in the packaging of this file.  Please review the
+** following information to ensure the GNU Lesser General Public License
+** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
+** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
+**
+** In addition, as a special exception, The Qt Company gives you certain additional
+** rights.  These rights are described in The Qt Company LGPL Exception
+** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
+**
+****************************************************************************/
+
+#include "qtlockedfile.h"
+
+/*!
+    \class QtLockedFile
+
+    \brief The QtLockedFile class extends QFile with advisory locking functions.
+
+    A file may be locked in read or write mode. Multiple instances of
+    \e QtLockedFile, created in multiple processes running on the same
+    machine, may have a file locked in read mode. Exactly one instance
+    may have it locked in write mode. A read and a write lock cannot
+    exist simultaneously on the same file.
+
+    The file locks are advisory. This means that nothing prevents
+    another process from manipulating a locked file using QFile or
+    file system functions offered by the OS. Serialization is only
+    guaranteed if all processes that access the file use
+    QtLockedFile. Also, while holding a lock on a file, a process
+    must not open the same file again (through any API), or locks
+    can be unexpectedly lost.
+
+    The lock provided by an instance of \e QtLockedFile is released
+    whenever the program terminates. This is true even when the
+    program crashes and no destructors are called.
+*/
+
+/*! \enum QtLockedFile::LockMode
+
+    This enum describes the available lock modes.
+
+    \value ReadLock A read lock.
+    \value WriteLock A write lock.
+    \value NoLock Neither a read lock nor a write lock.
+*/
+
+/*!
+    Constructs an unlocked \e QtLockedFile object. This constructor behaves in the same way
+    as \e QFile::QFile().
+
+    \sa QFile::QFile()
+*/
+QtLockedFile::QtLockedFile()
+    : QFile()
+{
+#ifdef Q_OS_WIN
+    m_semaphore_hnd = 0;
+    m_mutex_hnd = 0;
+#endif
+    m_lock_mode = NoLock;
+}
+
+/*!
+    Constructs an unlocked QtLockedFile object with file \a name. This constructor behaves in
+    the same way as \e QFile::QFile(const QString&).
+
+    \sa QFile::QFile()
+*/
+QtLockedFile::QtLockedFile(const QString &name)
+    : QFile(name)
+{
+#ifdef Q_OS_WIN
+    m_semaphore_hnd = 0;
+    m_mutex_hnd = 0;
+#endif
+    m_lock_mode = NoLock;
+}
+
+/*!
+    Returns \e true if this object has a in read or write lock;
+    otherwise returns \e false.
+
+    \sa lockMode()
+*/
+bool QtLockedFile::isLocked() const
+{
+    return m_lock_mode != NoLock;
+}
+
+/*!
+    Returns the type of lock currently held by this object, or \e QtLockedFile::NoLock.
+
+    \sa isLocked()
+*/
+QtLockedFile::LockMode QtLockedFile::lockMode() const
+{
+    return m_lock_mode;
+}
+
+/*!
+    \fn bool QtLockedFile::lock(LockMode mode, bool block = true)
+
+    Obtains a lock of type \a mode.
+
+    If \a block is true, this
+    function will block until the lock is acquired. If \a block is
+    false, this function returns \e false immediately if the lock cannot
+    be acquired.
+
+    If this object already has a lock of type \a mode, this function returns \e true immediately. If this object has a lock of a different type than \a mode, the lock
+    is first released and then a new lock is obtained.
+
+    This function returns \e true if, after it executes, the file is locked by this object,
+    and \e false otherwise.
+
+    \sa unlock(), isLocked(), lockMode()
+*/
+
+/*!
+    \fn bool QtLockedFile::unlock()
+
+    Releases a lock.
+
+    If the object has no lock, this function returns immediately.
+
+    This function returns \e true if, after it executes, the file is not locked by
+    this object, and \e false otherwise.
+
+    \sa lock(), isLocked(), lockMode()
+*/
+
+/*!
+    \fn QtLockedFile::~QtLockedFile()
+
+    Destroys the \e QtLockedFile object. If any locks were held, they are released.
+*/
--- /dev/null
+++ b/thirdparty/qtlockedfile/qtlockedfile.h
@@ -0,0 +1,74 @@
+/****************************************************************************
+**
+** Copyright (C) 2015 The Qt Company Ltd.
+** Contact: http://www.qt.io/licensing
+**
+** This file is part of Qt Creator.
+**
+** Commercial License Usage
+** Licensees holding valid commercial Qt licenses may use this file in
+** accordance with the commercial license agreement provided with the
+** Software or, alternatively, in accordance with the terms contained in
+** a written agreement between you and The Qt Company.  For licensing terms and
+** conditions see http://www.qt.io/terms-conditions.  For further information
+** use the contact form at http://www.qt.io/contact-us.
+**
+** GNU Lesser General Public License Usage
+** Alternatively, this file may be used under the terms of the GNU Lesser
+** General Public License version 2.1 or version 3 as published by the Free
+** Software Foundation and appearing in the file LICENSE.LGPLv21 and
+** LICENSE.LGPLv3 included in the packaging of this file.  Please review the
+** following information to ensure the GNU Lesser General Public License
+** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
+** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
+**
+** In addition, as a special exception, The Qt Company gives you certain additional
+** rights.  These rights are described in The Qt Company LGPL Exception
+** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
+**
+****************************************************************************/
+
+#ifndef QTLOCKEDFILE_H
+#define QTLOCKEDFILE_H
+
+#include <QFile>
+
+#if defined(Q_OS_WIN)
+#  if !defined(QT_QTLOCKEDFILE_EXPORT) && !defined(QT_QTLOCKEDFILE_IMPORT)
+#    define QT_QTLOCKEDFILE_EXPORT
+#  elif defined(QT_QTLOCKEDFILE_IMPORT)
+#    if defined(QT_QTLOCKEDFILE_EXPORT)
+#      undef QT_QTLOCKEDFILE_EXPORT
+#    endif
+#    define QT_QTLOCKEDFILE_EXPORT __declspec(dllimport)
+#  elif defined(QT_QTLOCKEDFILE_EXPORT)
+#    undef QT_QTLOCKEDFILE_EXPORT
+#    define QT_QTLOCKEDFILE_EXPORT __declspec(dllexport)
+#  endif
+#else
+#  define QT_QTLOCKEDFILE_EXPORT
+#endif
+
+class QT_QTLOCKEDFILE_EXPORT QtLockedFile : public QFile
+{
+public:
+    enum LockMode { NoLock = 0, ReadLock, WriteLock };
+
+    QtLockedFile();
+    QtLockedFile(const QString &name);
+    ~QtLockedFile();
+
+    bool lock(LockMode mode, bool block = true);
+    bool unlock();
+    bool isLocked() const;
+    LockMode lockMode() const;
+
+private:
+#ifdef Q_OS_WIN
+    Qt::HANDLE m_semaphore_hnd;
+    Qt::HANDLE m_mutex_hnd;
+#endif
+    LockMode m_lock_mode;
+};
+
+#endif // QTLOCKEDFILE_H
--- /dev/null
+++ b/thirdparty/qtlockedfile/qtlockedfile.pri
@@ -0,0 +1,13 @@
+INCLUDEPATH += $$PWD
+DEPENDPATH += $$PWD
+HEADERS += $$PWD/qtlockedfile.h
+SOURCES += $$PWD/qtlockedfile.cpp
+
+unix:SOURCES += $$PWD/qtlockedfile_unix.cpp
+win32:SOURCES += $$PWD/qtlockedfile_win.cpp
+
+win32:contains(TEMPLATE, lib):contains(CONFIG, shared) {
+    DEFINES += QT_QTLOCKEDFILE_EXPORT=__declspec(dllexport)
+}
+
+
--- /dev/null
+++ b/thirdparty/qtlockedfile/qtlockedfile_unix.cpp
@@ -0,0 +1,107 @@
+/****************************************************************************
+**
+** Copyright (C) 2015 The Qt Company Ltd.
+** Contact: http://www.qt.io/licensing
+**
+** This file is part of Qt Creator.
+**
+** Commercial License Usage
+** Licensees holding valid commercial Qt licenses may use this file in
+** accordance with the commercial license agreement provided with the
+** Software or, alternatively, in accordance with the terms contained in
+** a written agreement between you and The Qt Company.  For licensing terms and
+** conditions see http://www.qt.io/terms-conditions.  For further information
+** use the contact form at http://www.qt.io/contact-us.
+**
+** GNU Lesser General Public License Usage
+** Alternatively, this file may be used under the terms of the GNU Lesser
+** General Public License version 2.1 or version 3 as published by the Free
+** Software Foundation and appearing in the file LICENSE.LGPLv21 and
+** LICENSE.LGPLv3 included in the packaging of this file.  Please review the
+** following information to ensure the GNU Lesser General Public License
+** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
+** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
+**
+** In addition, as a special exception, The Qt Company gives you certain additional
+** rights.  These rights are described in The Qt Company LGPL Exception
+** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
+**
+****************************************************************************/
+
+#include "qtlockedfile.h"
+
+#include <string.h>
+#include <errno.h>
+#include <unistd.h>
+#include <fcntl.h>
+
+
+bool QtLockedFile::lock(LockMode mode, bool block)
+{
+    if (!isOpen()) {
+        qWarning("QtLockedFile::lock(): file is not opened");
+        return false;
+    }
+
+    if (mode == NoLock)
+        return unlock();
+
+    if (mode == m_lock_mode)
+        return true;
+
+    if (m_lock_mode != NoLock)
+        unlock();
+
+    struct flock fl;
+    fl.l_whence = SEEK_SET;
+    fl.l_start = 0;
+    fl.l_len = 0;
+    fl.l_type = (mode == ReadLock) ? F_RDLCK : F_WRLCK;
+    int cmd = block ? F_SETLKW : F_SETLK;
+    int ret = fcntl(handle(), cmd, &fl);
+
+    if (ret == -1) {
+        if (errno != EINTR && errno != EAGAIN)
+            qWarning("QtLockedFile::lock(): fcntl: %s", strerror(errno));
+        return false;
+    }
+
+
+    m_lock_mode = mode;
+    return true;
+}
+
+
+bool QtLockedFile::unlock()
+{
+    if (!isOpen()) {
+        qWarning("QtLockedFile::unlock(): file is not opened");
+        return false;
+    }
+
+    if (!isLocked())
+        return true;
+
+    struct flock fl;
+    fl.l_whence = SEEK_SET;
+    fl.l_start = 0;
+    fl.l_len = 0;
+    fl.l_type = F_UNLCK;
+    int ret = fcntl(handle(), F_SETLKW, &fl);
+
+    if (ret == -1) {
+        qWarning("QtLockedFile::lock(): fcntl: %s", strerror(errno));
+        return false;
+    }
+
+    m_lock_mode = NoLock;
+    remove();
+    return true;
+}
+
+QtLockedFile::~QtLockedFile()
+{
+    if (isOpen())
+        unlock();
+}
+
